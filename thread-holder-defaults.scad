// BOF

// NOSTL

include <libopenscad/pegboard-defaults.scad>;


$fn = 100;

guterman_500m_spool_width = 56;
guterman_500m_spool_inner_diameter = 7;
guterman_500m_spool_outer_diameter = 32.25;

bernina_bobbin_width = 7.5;
bernina_bobbin_outer_diameter = 22;
bernina_bobbin_inner_diameter = 6;


tray_height = (guterman_500m_spool_outer_diameter * 1/3) + (plate_thickness * 1.5);
tray_width = guterman_500m_spool_width + plate_thickness * 3;

guterman_500m_tray_depth = guterman_500m_spool_outer_diameter + plate_thickness * 3;
bernina_bobbin_tray_depth = bernina_bobbin_outer_diameter + plate_thickness * 3;

 //    debug_model = true;


// EOF

